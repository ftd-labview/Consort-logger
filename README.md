# Consort datalogger

A simple LabVIEW datalogger for a Consort multi-parameter analyser.



## Software Requirements

LabVIEW 2021 or later

## License

This project is licenced under the [EUPL Licence](LICENCE.md).

## Contact

For any questions or inquiries, please contact Dirk Geerts (d.j.m.geerts@tudelft.nl).
